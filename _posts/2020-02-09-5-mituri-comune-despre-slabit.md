---
title: 5 mituri comune despre slabit
date: 2020-02-09 19:01:00 +00:00
tags:
- sfaturi
- dieta
- slăbit
- mituri
layout: post
author: Gabriela
modified_time: '2020-02-22T06:59:55.868+02:00'
thumbnail: https://1.bp.blogspot.com/-6MHNbNcU3DU/XkBW9kKOyzI/AAAAAAAAD-I/nbEgICKLv8UeSBjItQ3Np4fd7LyGkGvMACLcBGAsYHQ/s72-c/Mituri-cloud-word.jpg
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-1399022611457665829
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/5-mituri-comune-despre-slabit.html
---

### 

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-6MHNbNcU3DU/XkBW9kKOyzI/AAAAAAAAD-I/nbEgICKLv8UeSBjItQ3Np4fd7LyGkGvMACLcBGAsYHQ/s1600/Mituri-cloud-word.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="768" data-original-width="1024" height="480" src="{{ site.url }}/assets/2020-02-09-image-0000.jpg" width="640"/></a></div>

<div class="separator" style="clear: both; text-align: center;"></div>

### 

### 

### Mit: vei fi în regulă cu o dietă bogată în proteine / scăzută în carbohidrați&nbsp;

<div style="text-align: left;"><i><b>Fapt: </b>acest tip de dietă își propune să asigure cea mai mare parte din cantitatea zilnică necesară de calorii din alimentele proteice, cum ar fi carnea, produsele lactate și ouăle, cu o foarte mică parte luată din alimente care conțin o mulțime de carbohidrați (pâine, paste, cartofi și fructe). Cea mai mare problemă cu această dietă este că multe alimente comune și populare sunt interzise, ceea ce te face ușor să devii plictisita sau frustrata. Când plictiseala și frustrarea se instalează, renunțarea nu e niciodată departe. O altă problemă este lipsa de nutrienți care provin din alimentele bogate in carbohidrați, în timp ce e permis să mănânci șuncă și brânză care cresc nivelurile de colesterol.</i></div>

<div style="text-align: left;"><br/></div>

<div style="text-align: left;"><i>Concluzia este că dietele bogatele în proteine / carbohidrați putini duc la o scădere rapidă în greutate la unii oameni, dar nu este vorba doar de grăsime care dispare. Oamenii pierd, de asemenea, o parte din masa lor musculare&nbsp; și o mulțime de apă din cauza ca rinichii lucrează mai greu pentru a scăpa corpul de deșeuri în exces din proteine și grăsimi.</i></div>

<div style="text-align: left;"><i><br/></i></div>

<div style="text-align: left;"><i>Stresul adăugat la rinichi nu este sănătos deloc și, pe termen lung, va duce la deshidratare, dureri de cap, greață și amețeli. Înainte de a vă angaja într-o dietă pe termen lung, ați face bine să o discutați cu medicul dumneavoastră. Și dacă sunteți într-adevăr dornici de dieta, încercați o dietă echilibrată în primul rând, pentru că vă va ajuta să pierdeți în greutate fără a vă afecta sănătatea.</i></div>

<div style="text-align: left;"><br/></div>

### Mit: pierderea permanentă în greutate poate fi realizată cu diete pe termen scurt.

___Fapt__: acesta este un alt mit comun în rândul celor care vor sa slăbească. Ca orice dietă care îți spune să tai alimentul X sau Y din&nbsp; mesele zilnice, dietele pe termen scurt nu sunt sănătoase pentru tine. Piatra de temelie a oricărei diete este furnizarea catre corp a tuturor caloriilor, proteinelor și mineralelor de care are nevoie. Dieta care taie alimentele fără a ține cont de nevoile corpului tău nu poate fi sănătoasă pentru tine. Diete de pe o zi pe alta nu aduc o pierdere de greutate permanentă, deoarece oamenii își vor dori întotdeauna alimente interzise și vor sfârși&nbsp; prin încălcarea regulilor și renunțarea la dieta._  
_  
__Cheia pentru o pierderea de greutate permanentă este mai mult efort fizic și evitarea consumului de alimente care îngrașă, fără a hrăni corpul. Studiile au confirmat importanța exercițiilor fizice și a unei diete bine echilibrate._  
  

### Mit: Alimentele cu conținut redus de grăsimi sunt întotdeauna o alegere bună.

___Fapt__: singurele alimente cu conținut scăzut de grăsimi care sunt, de asemenea, scăzute în calorii sunt fructele. Restul grupului uriaș de alimente&nbsp; cu conținut scăzut de grăsimi sau fără grăsimi nu reprezintă o alegere sigură dacă ții dietă. Aceste produse pot să aiba adăugat zahăr, amidon sau făină pentru a le face să aibă un gust mai bun, ceea ce înseamnă calorii suplimentare care nu fac parte din dieta dumneavoastră. Deci, asigurați-vă că verificați lista de ingrediente de pe pachet ori de câte ori vă decideți să cumpărați._  
_  
_  

### Mitul: trecerea la alimente vegetariene ma va ajuta să pierd în greutate și să rămân sănătos.

___Fapt__: știu că e greu de înghițit, dar nicio dieta nu va funcționa pentru tine dacă nu te ocupi de ce mananci și te asiguri că tot ce trece prin gura ta e genul potrivit de mâncare. Cheia aici este să fii absolut sigur că dieta vegetariană va da corpului tău toti nutrienții și caloriile de care are nevoie. Consumul de grămezi de alimente care nu fac nimic pentru tine din punct de vedere nutrițional, în afara, poate de umplerea stomacului, vă poate aduce o mulțime de probleme în viitor._  
_  
__Deoarece plantele tind să aibă o concentrație mai mică de nutrienți decât carnea, va trebui să mănânci mai multă hrană decât înainte pentru a compensa această diferență. Ca să nu mai spun că o dietă strict vegetariană nu-ți va aduce suficientă vitamina B12, vitamina D, fier, calciu și zinc. Aceste substanțe sunt luate în principal din produse lactate și ouă într-o dietă normală, dar vegetarienii adevărați nu le pot avea. Va trebui să vă bazați pe câteva(și mai puțin frecvente) legume care pot_  
_&nbsp;furniza aceste substanțe._  
  

### Mitul: produsele lactate cauzează probleme cardiace și te îngrașă.

___Fapt__: ei bine, datorită medicinei moderne toată lumea știe că a mânca o mulțime de produse lactate este o cale sigură spre hipertensiune arterială și accident vascular cerebral, deși un grup mare de oameni din Franța refuză să moară în ciuda cantităților mari de brânză și vin pe care le consumă._  
_  
__Produsele lactate sunt principala sursă de calciu care vă menține oasele puternice și sănătoase. Este absolut esențial pentru copii și nu poate fi trecut cu vederea de către adulți. Produsele lactate conțin, de asemenea, vitamina D, care este esențială pentru menținerea nivelurilor normale de calciu și fosfor în sânge._  
_  
__Dacă nu doriți să luați în greutate din produse lactate, atunci alegeți mărci cu conținut scăzut de grăsimi sau fără grăsimi.&nbsp; Dacă aveți intoleranță la lactoză, beți soia sau lapte de orez. Nu are același gust ca laptele adevărat, dar este încă bun și hrănitor._  
_  
__Untul și margarina sunt singurele produse lactate de care ar trebui să-ți faci griji. Ele conțin niveluri mari de grăsime. Totuși, dacă nu te poți descurca fără ele, măcar mănâncă unt. Margarina este o grăsime supra-procesată si nu este cea mai sănătoasă soluție pentru tine. Untul poate fi un pic mai gras, dar cu siguranță este mai sănătos dintre aceste două._