---
title: Teff, cereale fara gluten
date: 2020-02-22 21:31:00 +00:00
tags:
- fara gluten
- Teff
- faina
- injera
layout: post
author: Gabriela
modified_time: '2020-02-22T23:31:11.350+02:00'
thumbnail: https://1.bp.blogspot.com/-p7SF1jlcYAQ/XlGZ1VePbuI/AAAAAAAAEIg/0Po5_PCxGfIAsFnaZkRyk_hZQxpe9JhXQCKgBGAsYHg/s72-c/IMG_20200222_225931.jpg
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-7591415968525711765
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/teff-cereale-fara-gluten.html
---

## &nbsp;Ce este teff-ul?

<div class="separator" style="clear: both; text-align: center;"></div>

  
Faina de teff este obținută din cerealele minuscule ale unei cereale foarte frecvente în Etiopia și Eritrea, unde este un&nbsp;ingredient prețios&nbsp;al bucătăriei locale.  
  
&nbsp;Cu mirosurile sale caracteristice, este o făină bogată în&nbsp;vitamine&nbsp;, fibre și calciu și este perfectă pentru aluaturi, cum ar fi produse de patiserie, biscuiți și clătite!  
  

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-p7SF1jlcYAQ/XlGZ1VePbuI/AAAAAAAAEIg/0Po5_PCxGfIAsFnaZkRyk_hZQxpe9JhXQCKgBGAsYHg/s1600/IMG_20200222_225931.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" data-original-height="518" data-original-width="645" height="160" src="{{ site.url }}/assets/2020-02-22-image-0000.jpg" width="200"/></a></div>

  
__Teff__ (&nbsp;_Eragrostis tef&nbsp;_) este o cereală al cărei bob, de diferite culori (alb, roșu sau maro închis), se caracterizează printr-o dimensiune foarte mică, mult mai mică decât o sămânță de mac (este nevoie de 3000 pentru a face un gram ...) .  
Și tocmai pentru dimensiunile minuscule, populațiile semi-nomade din&nbsp;Eritrea și Etiopia&nbsp;au făcut din el un element la baza dietei lor: de fapt, doar o jumătate de kilogram este suficientă pentru a produce, în doar 2 săptămâni, o tonă.  
&nbsp;În plus, teff-ul se poate găti într-un timp foarte scurt și, prin urmare, necesită mai puțin combustibil, element care nu trebuie subestimat într-o economie care, cu siguranță, nu este confortabilă.  
  
Comparativ cu alte cereale, teff are un procent mai mare de tărâțe și germeni: și, datorită mărimii boabelor, este practic imposibil să separi partea externă de cea internă, astfel încât făina obținută este&nbsp;numai integrală.&nbsp;Bogat în aminoacizi, conține numeroase vitamine, precum și un aport semnificativ de calciu.&nbsp;La aceasta se adaugă absența acidului fitic, capabil să contracareze absorbția elementelor importante pentru corpul nostru.&nbsp;Teff este de asemenea bogat în fibre insolubile, esențiale pentru bunăstarea intestinului nostru și pentru reglarea absorbției zaharurilor.  
  
Caracteristicile sale organoleptice sunt cele care îl fac special.&nbsp;Încercați să mirosiți o pungă de teff, imediat după deschiderea acesteia: veți fi înfășurat într-o&nbsp;aromă&nbsp;delicioasă de&nbsp;malț&nbsp;, care vă poate aminti de Chai, ceaiul indian condimentat.&nbsp;Teff are un gust destul de&nbsp;dulce și o aromă ușor prăjită&nbsp;care amintește de nuci, cacao și crusta aromată de pâine.  
  

### Făina de Teff în bucătărie

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-ktcsZ-Qxg0w/XlGZ9SDAL6I/AAAAAAAAEIk/IIjjkFV2wBU7VI9eZQoSHHvMZm6R_ltbQCKgBGAsYHg/s1600/IMG_20200222_230951.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="616" data-original-width="646" height="190" src="{{ site.url }}/assets/2020-02-22-image-0001.jpg" width="200"/></a></div>

  
Un element indispensabil pentru prepararea injerei,&nbsp;pâinea tradițională etiopiană&nbsp;, constând dintr-un fel de clătită spongioasă fermentată natural, făina de teff este perfectă pentru prepararea unor&nbsp;produse scurte și a unor produse de patiserie, biscuiți, clătite și pâine&nbsp;, cărora le oferă o aromă delicioasă, dar este excelent și pentru îngroșarea supelor, sosurilor și cremelor.&nbsp;Față de amidonul de porumb, are o întărire mai lentă, ceea ce înseamnă că dă un produs puțin mai moale decât cel obținut cu amidon de porumb.&nbsp;În rețetele care necesită structură, făina de teff&nbsp;poate înlocui până la 25% din făina de bază, în timp ce poate fi folosit și în procente mai mari pentru deserturi moi sau crepes.&nbsp;În gătit, de fapt, tinde să devină ușor gelatinoasă.  
  
  

### Două tipuri de făină

  
Există două tipuri de făină de teff pe piață.&nbsp;Faina alba sau ivorie&nbsp;, cea mai valoroasă, are un gust dulce care&nbsp;amintește cumva de castane.&nbsp;Parfumul său de crustă de pâine îl face deosebit de apreciat de celiaci și este potrivit pentru a face pâine rapidă cu puțină făină.&nbsp;Făina întunecată&nbsp;are arome si note rustice, amintind de nucile uscate;&nbsp;perfect pentru preparate care folosesc nuci sau alune, precum și ciocolată, ale căror arome&nbsp; sunt îmbunătățite, este de asemenea excelent pentru preparate cu condimente și ingrediente afumate.&nbsp;Fiind grâu integral, făina de teff are un&nbsp;conținut ridicat de ulei&nbsp;: este recomandat să o păstrați în locuri frigorifice sau chiar la congelator.  
  

### Calități nutriționale

  
Teff-ul este o cereală fără&nbsp;gluten&nbsp;, de aceea este potrivită și pentru cei care suferă de&nbsp;boală celiacă&nbsp;, este de asemenea foarte bogată în&nbsp;lizină&nbsp;, un&nbsp;&nbsp;aminoacid esențial care lipsește aproape toate celelalte cereale.  
  
  
Având în vedere indicele glicemic scăzut, este potrivit și pentru persoanele care suferă de&nbsp;diabet.&nbsp;De asemenea, este o sursă bună de vitamine&nbsp;B1&nbsp;și&nbsp;B6&nbsp;și săruri minerale precum&nbsp;mangan&nbsp;,&nbsp;fier&nbsp;și&nbsp;magneziu&nbsp;.  
  

### Teff în bucătărie

  
Faina de teff poate fi folosită pentru a face&nbsp;pâine&nbsp;,&nbsp;pizza,&nbsp;dar și deserturi, prăjituri sau&nbsp;biscuiți&nbsp;, pe scurt toate produsele de preparare a pâinii și patiserie.  
  
Cerealele de teff, atunci când sunt disponibile, pot fi gătite fierte în puțină apă sau folosite crude puțin așa cum se face cu&nbsp;semințe oleaginoase&nbsp;&nbsp;în&nbsp;salate&nbsp;.  
  
Teff are o putere mare de îngroșare, așa că acordați atenție dozelor, mai ales dacă îl utilizați în&nbsp;supe&nbsp;.  
  
În Etiopia, făina de teff este folosită în principal pentru a face&nbsp;injera&nbsp;, un fel de pâine plină care însoțește&nbsp;tocanele de&nbsp;carne și mezeluri.  
  
__Injera__ este o pâine acră folosită în bucătăria etiopiană și eritreană, care este mai groasă decât o clătită, dar mai subțire decât o turtă și are un gust delicios acru.&nbsp;În bucătăriile etiopiene și eritreene, mâncărurile cu legume, linte sau carne sunt servite deasupra injerei, iar mâncarea este mâncată cu mâinile tale, folosind injera pentru a scoate mâncarea.  
  
În timp ce injera tradițională folosește exclusiv făina de teff, se poate folosi si o combinație de făină de teff și grâu, motiv pentru care vă prezint această rețetă, care folosește făină de grâu cu făină de teff.  
  
  

### Ingrediente

  

*   2 cani faina de teff
*   2 cană făină de grâu
*   1/2 lingurita sare
*   5 căni de apă călduță

  
  

#### Pași de urmat

  
  
  
<ol><li>Într-un bol mare, se pun făina și sarea.Adăugați apa, amestecând până la omogenizare.<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-48coNjkt-9U/XlGazIqRobI/AAAAAAAAEI0/A6enyLR0kG4HPsEjpxFNIn1FhC2f_HhvQCKgBGAsYHg/s1600/IMG_20200222_230521.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="1068" data-original-width="1600" height="212" src="{{ site.url }}/assets/2020-02-22-image-0002.jpg" width="320"/></a></div></li><li>Acoperiți cu un prosop de hârtie și lăsați să se odihnească peste noapte.<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-Eyh2uiQ0drY/XlGbD0E3u8I/AAAAAAAAEJA/RMa__WBfcGAYSn_0G1c_L_LYjNP9eGQLgCKgBGAsYHg/s1600/IMG_20200222_231139.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="1069" data-original-width="1600" height="212" src="{{ site.url }}/assets/2020-02-22-image-0003.jpg" width="320"/></a></div></li><li>Agitați ușor amestecul cu o lingură de lemn dimineața (ar trebui să existe bule care deja se formează la suprafață, iar apa în fermentare ar fi trebuit să se ridice până la vârf).<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-81WAZp7kvIM/XlGbNrl2FPI/AAAAAAAAEJE/lzlaDfj81AMzy9D2XF8SSdKGtCldvpbHACKgBGAsYHg/s1600/IMG_20200222_230753.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="168" data-original-width="253" height="211" src="{{ site.url }}/assets/2020-02-22-image-0004.jpg" width="320"/></a></div></li><li>Se acoperă din nou și se lasă să stea la temperatura camerei, încă o noapte.&nbsp;Repetați procesul de agitare a amestecului în dimineața următoare și apoi încă o rundă de odihnă peste noapte.</li><li>După 3 până la 4 zile, injera ta ar trebui să miroasă acru și să fie plină de bule, ceea ce înseamnă că este gata!&nbsp;Amestecați compoziția până la omogenizare.<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/--3Bu_lzo0iQ/XlGbb1wAVMI/AAAAAAAAEJI/bsvuzfum84IsPNSWzLx0DXJ9_AeUVNVTQCKgBGAsYHg/s1600/IMG_20200222_231320.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="184" data-original-width="275" height="212" src="{{ site.url }}/assets/2020-02-22-image-0005.jpg" width="320"/></a></div></li><li>Încălziți putin ulei într-o tigaie mare, cu fundul dublu, la foc mediu. Adăugați aproximativ 1/3 cană de aluat în tigaie, formând un strat subțire in tigaie.&nbsp;Se gătește până când se formează bule pe toată suprafața platului (nu se răstoarnă; se gătește doar pe o parte).</li><li>Cu ajutorul unei spatule metalice, scoateți injera și transferați-o pe o tavă.<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-EFs9JU9SG64/XlGcCJYQxoI/AAAAAAAAEJc/ieVH9l4gA7gzN6Zt0EE4anFsUll8M942ACKgBGAsYHg/s1600/IMG_20200222_232346.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="397" data-original-width="529" height="240" src="{{ site.url }}/assets/2020-02-22-image-0006.jpg" width="320"/></a></div></li><li>Repetați până când se folosește toata compoziția, adăugând ulei în tigaie, atât cât e nevoie.<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-z_zlthINBuk/XlGbkF6vF_I/AAAAAAAAEJM/3sj49XWikowR7jJ2BsMvqRJKxgNYz0baACKgBGAsYHg/s1600/IMG_20200222_230252.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="478" data-original-width="637" height="240" src="{{ site.url }}/assets/2020-02-22-image-0007.jpg" width="320"/></a></div></li><li>Serviți cu legume picante, linte sau&nbsp;preparate&nbsp;din&nbsp;carne&nbsp;la alegere.</li></ol>  
  