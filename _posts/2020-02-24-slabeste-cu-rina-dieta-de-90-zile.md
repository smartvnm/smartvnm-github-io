---
title: Slăbește cu Rina, dieta de 90 zile
date: 2020-02-24 00:38:00 +00:00
tags:
- meniu
- dieta
- Rina
- slăbit
layout: post
author: Gabriela
modified_time: '2020-02-24T02:40:03.088+02:00'
thumbnail: https://lh3.googleusercontent.com/-egpj695GgWc/XlLuxVCYSyI/AAAAAAAAEJ0/vksxYgexS2MqpENYtESW5XcLPIF3lCDtgCLcBGAsYHQ/s72-c/1582493378304221-1.png
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-1211971574507345811
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/slabeste-cu-rina-dieta-de-90-zile.html
---

<div class="separator" style="clear: both; text-align: center;"><br/></div>

## &nbsp; &nbsp; Istoricul dietei Rina

  
&nbsp;   
  
&nbsp; &nbsp; Nu există date specifice care să arate originea acestei diete, unele voci susțin că numele este datorat&nbsp; unui medic sportiv american care a publicat un plan alimentar disociat într-o revistă numită Rina, prin anul 2000.  
  
&nbsp;   
  
&nbsp; &nbsp; Pe de altă parte, se afirmă că dieta Rina este&nbsp; rezultatul unei cercetări privind o metodă eficientă de hrănire menținută de revista Rina în ultimii 15 ani, dezvoltată de autorii Breda Hrobat și Mojca Poljansek sub supraveghere Prof. Dr. Ștefan Celan. În 2004, a fost publicată prima versiune a cărții despre dieta Rina, care a avut un succes atât de mare, încât ulterior a fost tradusă în 10 limbi publicate în Slovacia, Cehia, Croația, Lituania, Rusia, Bosnia și Herțegovina, Serbia, România, Bulgaria și Germania.  
  
&nbsp;   

<div class="separator" style="clear: both; text-align: center;"><a href="https://lh3.googleusercontent.com/-egpj695GgWc/XlLuxVCYSyI/AAAAAAAAEJ0/vksxYgexS2MqpENYtESW5XcLPIF3lCDtgCLcBGAsYHQ/s1600/1582493378304221-1.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"> <img border="0" src="https://lh3.googleusercontent.com/-egpj695GgWc/XlLuxVCYSyI/AAAAAAAAEJ0/vksxYgexS2MqpENYtESW5XcLPIF3lCDtgCLcBGAsYHQ/s1600/1582493378304221-1.png" width="400"/> </a> </div>

  
&nbsp; &nbsp; Deși originile sale sunt de obicei oarecum inexacte, impactul pe care l-a provocat în societate l-a încadrat ca dieta preferată a multor oameni. Această dietă&nbsp; care promite pierderea a 20 kg în trei luni,&nbsp; este o dietă promițătoare, sănătoasă și echilibrată,a cărei popularitate a crescut de-a lungul anilor.  
  
&nbsp;   
  

## &nbsp; &nbsp; Ce este Dieta Rina?

  
&nbsp;   
  
&nbsp; &nbsp; Rina este o dietă disociată repetitivă, ceea ce înseamnă că este necesar un aport zilnic dintr-un anumit tip de aliment, această dietă trebuind urmată timp de 90 de zile la rând, repetând meniuri diferite timp de 4 zile. Este o dietă care se folosește exclusiv pentru a slăbi, subliniind că este cunoscută și sub denumirea de dieta Rina 90 sau dieta de 90 de zile, în care o zi pe lună este programată ca zi de post.  
  
&nbsp;   
  
&nbsp; &nbsp; De asemenea, se evidențiază faptul că dieta Rina este o dietă care nu se concentrează mult pe calorii, dar pune un accent mai mare asupra tipului de alimente care ar trebui consumate în fiecare zi, fără a lăsa deoparte importanța activității fizice constante. Această dietă incredibilă este considerată una dintre cele mai sănătoase și mai eficiente, care poate schimba greutatea în doar 90 de zile, reușind să o mențină în timp. În plus, dieta Rina ajută la reglarea metabolismului, la creșterea stării de sănătate și a stării de bine generale, precizând că după câteva săptămâni vă veți regăsi plini de energie, sănătate și bună dispoziție.  
  
&nbsp;   
  

## &nbsp; &nbsp; Cui se adresează?

  
&nbsp;   
  
&nbsp; &nbsp; Dieta Rina se adresează în primul rând acelor persoane obeze ( cu exces de greutate ), subliniind că obezitatea este considerată una dintre cele mai cunoscute boli, care afectează un număr mare de țări. Pe lângă faptul că este strâns legată de obiceiurile alimentare, dieta nu înseamnă doar lupta pentru un aspect fizic, ci și la combaterea posibilelor probleme de sănătate cauzate de obezitate. Din acest motiv, există o mulțime de diete, cu toate acestea, ceea ce face dieta Rina foarte promițătoare este că, în funcție de angajamentul oamenilor, se pierde&nbsp; până la 25 kg. Fără îndoială, o cantitate considerabilă de kilograme pentru care ar merita să depunem un efort.  
  
&nbsp;   

<div class="separator" style="clear: both; text-align: center;"><a href="https://lh3.googleusercontent.com/-Nkx8i9cQQ7c/XlLy9gOvSJI/AAAAAAAAEKU/391n8vT0OX8LD5MUKPlihT1zGmLN7hAlACLcBGAsYHQ/s1600/woman-1979272_1920.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img alt="Gătit în familie" border="0" class="" data-original-height="1280" data-original-width="1920" height="342" src="https://lh3.googleusercontent.com/-Nkx8i9cQQ7c/XlLy9gOvSJI/AAAAAAAAEKU/391n8vT0OX8LD5MUKPlihT1zGmLN7hAlACLcBGAsYHQ/s1600/woman-1979272_1920.jpg" title="Gătit în familie" width="513"/></a></div>

  
  
&nbsp; &nbsp; Dieta Rina se adresează, de asemenea, celor care nu au mult timp să gătească datorită unei rutine zilnice solicitante, astfel încât alimentele care fac parte din această dietă sunt făcute cu rețete simple, dar gustoase, deoarece acestea sunt combinate corect și se bazează pe produse ușor accesibile în magazine. Unele alimente pot fi gătite, apoi introduse în congelator și pot fi consumate după câteva zile. De asemenea, ai posibilitatea să testezi mâncarea care pare cea mai apetisantă, dacă aa se înscrie în programul stabilit și că are o combinație eficientă de alimente.  
  
&nbsp;   

<div class="separator" style="clear: both; text-align: center;"><a href="https://lh3.googleusercontent.com/-GDGVeKUhc3E/XlLy-u-u0eI/AAAAAAAAEKc/nMwxta0uNZ8P3ElgenWFRsg95CkaTHyEgCLcBGAsYHQ/s1600/nci-vol-2463-150.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" class="imageResizeTarget" data-original-height="900" data-original-width="1350" height="213" src="https://lh3.googleusercontent.com/-GDGVeKUhc3E/XlLy-u-u0eI/AAAAAAAAEKc/nMwxta0uNZ8P3ElgenWFRsg95CkaTHyEgCLcBGAsYHQ/s1600/nci-vol-2463-150.jpg" width="320"/></a></div>

  
  
&nbsp; &nbsp; Multi oameni obezi care au încercat diferite diete se tem de faimoasa zonă a efectului de revenire sau a efectului yo-yo, care provoacă recidive după ce a scăzut ușor greutatea sau au atins obiectivul stabilit, dar această dietă garantează un succes total evitând efectul yo-yo, deoarece acesta este un program prin care multe persoane au găsit modalitatea eficientă de a asculta dorințele corpului lor, schimbandu-si dieta și fiind mai activi din punct de vedere fizic. Deducem că dieta Rina nu este de fapt o dietă, ci este calea către un mod diferit de a mânca, sănătos, deoarece mesele echilibrate sunt oferite fără&nbsp; să-ți fie neapărat foame.  
  
&nbsp;   
  

## &nbsp; &nbsp; Cum funcționează dieta?

  
&nbsp;   
  
&nbsp; &nbsp; Dacă sunteți curioși să știți cum funcționează această dietă, primul lucru&nbsp; care ar trebui menționat este că dieta se bazează pe un ciclu repetitiv de 4 zile, pe parcursul celor 90 de zile stabilite. Tocmai aceasta este particularitatea care face dieta ușor de urmărit. Noțiunea de bază este foarte simplă, trebuie să consumați un anumit tip de mâncare fără a o amesteca cu altele timp de o zi. Luând în considerare&nbsp; studiile bazate pe compoziția alimentelor, proteinelor, grăsimilor și carbohidraților, etica profundă a dietei Rina și a tuturor celorlalte diete disociate este aceea că acestea nu ar trebui să combine anumite alimente.  
  
&nbsp;   
  
&nbsp; &nbsp; Combinarea corectă a diferitelor alimente într-o masă face posibilă&nbsp; descompunerea și asimilarea eficientă a nutrienților și, prin urmare, iar metabolismul se îmbunătățește dramatic, sistemul imunitar este consolidat și, de asemenea, se îmbunătățește condiția mentală și fizică.  
  
&nbsp;   
  
&nbsp; &nbsp; Științific vorbind, dieta Rina se concentrează pe incapacitatea umană de a prelucra de fapt mai multe tipuri de alimente simultan. Multe dintre problemele noastre digestive și problemele de sănătate ulterioare provin din faptul că amestecăm și potrivim tot felul de alimente într-o porție sau în aceeași zi.  
&nbsp;Acesta este motivul pentru care Rina disociază alimentele.  
  

## &nbsp; &nbsp;&nbsp; &nbsp; Ce se caută prin separarea alimentelor pe grupe?

  
&nbsp;   
  
&nbsp; &nbsp; Pentru experți, răspunsul este foarte simplu și este că alimentele consumate într-un mod mixt - așa cum suntem obișnuiți - necesită un efort mai mare pentru organism în momentul în care dorim să le digerăm, însă dacă le consumăm în mod ordonat și izolat crește optimizarea procesului metabolic. Aceasta înseamnă că dacă într-o zi mâncăm doar proteine, organismul le va digera mult mai bine, în plus, metabolismul este modificat prin accelerarea și motivarea unei pierderi rapide în greutate atunci când alimentele sunt disociate.  
  
&nbsp;   
  
&nbsp; &nbsp; În ceea ce privește grupurile nutriționale alimentare, vă puteți informa în profunzime despre care sunt cele care vă oferă cele mai mari beneficii, în funcție de nevoile dvs. de bază. Aveți la dispoziție clasificarea alimentelor în grupurile principale și modul în care acestea acționează în organism, trebuie doar să le alegeți pe cele potrivite.  

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-Rf-TrWi5wJU/XlMXSPIQWhI/AAAAAAAAELg/-Wg8un7IQQAv50NHxDxm9FptaC5RxrVeACKgBGAsYHg/s1600/IMG_20200224_021802.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img alt="Tabel alimente dieta Rina" border="0" data-original-height="551" data-original-width="419" src="https://1.bp.blogspot.com/-Rf-TrWi5wJU/XlMXSPIQWhI/AAAAAAAAELg/-Wg8un7IQQAv50NHxDxm9FptaC5RxrVeACKgBGAsYHg/s1600/IMG_20200224_021802.jpg" title=""/></a></div>

  
&nbsp;   
  
&nbsp; &nbsp; Există 4 principii clare în dieta Rina:  
  
&nbsp;   
  
&nbsp; &nbsp; __Principiul 1 __: Indiferent ce tip de mâncare mănânci într-o zi, ar trebui să ai numai fructe pentru micul dejun. Acest lucru ar putea suna puțin ciudat, dar fructele vă ajută sistemul digestiv să funcționeze corect. Indiferent ce aveți pentru prânz sau cină pe parcursul celor 90 de zile, micul dejun de dimineață ar trebui să conțină doar fructe.  
  
&nbsp;   
  
&nbsp; &nbsp; __Principiul 2 __: Trebuie să păstrați un program cu ce și când mâncați. Dieta Rina vă permite să luați micul dejun între orele 7:00 și 11:00, prânzul între 12:00 și 14:00 și cina între 18:00 și 20:00 Nu există gustări intermediare între principal trei mese și fără cină mai târziu de 20:00  
  
&nbsp;   
  
&nbsp; &nbsp; __Principiul 3__ : Pe parcursul celor 90 de zile de dietă, cina trebuie să fie jumătate din cantitatea de mâncare pe care ai avut-o pentru prânz.  
  
&nbsp;   
  
&nbsp; &nbsp; __Principiul 4__: bea multă apă toată ziua și încearcă să faci exerciții cardio cel puțin o dată la două zile.  
  
&nbsp;   
  
&nbsp;   

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-F6GZj_1PXQI/XlMa1xwHBEI/AAAAAAAAEL0/1AkzX8nE5QIT5Vo6qWpygkx1l7TxT29IQCKgBGAsYHg/s1600/fruit-2305192_1920.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="900" data-original-width="1600" height="179" src="https://1.bp.blogspot.com/-F6GZj_1PXQI/XlMa1xwHBEI/AAAAAAAAEL0/1AkzX8nE5QIT5Vo6qWpygkx1l7TxT29IQCKgBGAsYHg/s320/fruit-2305192_1920.jpg" width="320"/></a></div>

Dieta Rina nu este o dietă săracă în carbohidrați,&nbsp; deoarece sunt prezenți în legume și fructe care, după cum vom vedea mai târziu, sunt încă incluse. În plus, această dietă disociată nu are contraindicații.  

<div class="separator" style="clear: both; text-align: center;"></div>

  
&nbsp;   

<div style="background: #e8fff3;"><h2>&nbsp; &nbsp; Meniuri din dieta rina</h2><br/>&nbsp; <br/><br/>&nbsp; &nbsp; În legătură cu modul exact de funcționare al dietei, vă ofer un model care evidențiază alimentele care pot fi consumate zilnic.<br/><br/>&nbsp; <br/><br/><h3>&nbsp; &nbsp; Ziua 1 - ziua proteinelor</h3><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Pentru început, primul lucru pe care trebuie să-l avem în vedere este că în prima zi trebuie să fim clari în legătură cu obiectivele care ne-au determinat să începem dieta, trebuie să ne angajăm și să ne stabilim obiective reale, este de asemenea indicat să ne adaptăm la o rutină de activitate fizică care ne va ajuta să îmbunătățim dieta, obținând o stare de bine fizic și de sănătate.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Mic dejun</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Este recomandabil să începeți ziua cu consumul oricăruia dintre aceste alimente în prima zi, aveți o listă cu următoarele:<br/><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Cafea sau ceai fără adăugare de zahăr, cu 150 de grame de fructe la alegere, care pot fi, de exemplu, 2 mere, de preferință fierte, 2 banane mici sau, alternativ, una mare sau 2 piersici mici. Pentru&nbsp; gust&nbsp; puteți adăuga un vârf de scorțișoară dacă decidem să mâncăm fructe fierte.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Prânz</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; În acest moment al zilei puteți consuma alimente cu o încărcătură nutrițională mai mare: carne albă, roșie sau de vițel, vită sau pui, este o alegere care nu trebuie să depășească 300 gr, ouăle fierte sunt de asemenea o opțiune dar nu mai mult de 3, puteți consuma, de asemenea, 200 gr brânză cu conținut scăzut de grăsimi. În cazul în care carnea nu este pe placul dvs. sau dacă doriți altceva, puteți mânca pește alb, cum ar fi merluciu, cod&nbsp; păstrând&nbsp; porția în limita a 200 gr. La toate acestea puteți adăuga o salată verde asezonată doar cu suc de lămâie și oțet.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Cină</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; La cină putem repeta ceea ce este permis pentru prânz, dar reducând porțiile la jumătate, pentru carne nu va mai fi de 300 de grame, ci de 150 de grame, iar brânza de 100 de grame. Diferența este compensată de creșterea porției de legume.<br/><br/>&nbsp; <br/><br/><h3>&nbsp; &nbsp; Ziua 2 - fibre</h3><br/>&nbsp; <br/><br/>&nbsp; &nbsp; În&nbsp; cea de-a doua zi putem opta pentru consumul de „amidon”, ne referim la toate acele alimente care sunt consumate în aceeași stare în care au fost cultivate, cum ar fi cartofi, orez, fasole, mazăre , boabe de porumb, printre altele. Așadar, în această a doua zi, puteți consuma orez sau boabe, deoarece se consumă în același mod în care au fost cultivate, dar nu și paste, de exemplu - pentru că sunt un aliment procesat și făcut din diverse ingrediente.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Mic dejun</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Pentru micul dejun, puteți opta pentru aceleași alternative prezentate în ziua 1.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Prânz</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Pranzul ar trebui să fie întotdeauna principalul fel al zilei, astfel încât să puteți consuma 80 de grame de orez - alb sau maro - puteți adăuga 100 de grame de leguminoase la alegere, de exemplu, mazăre proaspătă sau fasole. Orezul poate fi înlocuit cu grâu spelta, fulgi de ovăz sau quinoa. Puteți alătura o porție de salată verde garnisită cu oțet de cidru de lămâie și mere sau cu o porție de legume la grătar din care să alegeți, poate fi dovlecel, vinete, roșii sau ardei.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Cină</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; De asemenea, porția de orez cuprinsă între 100 de grame și 50 de grame este înjumătățită, iar porțiunea de leguminoase este înjumătățită. Cantitățile de salată și legume rămân neschimbate. În timpul mesei, însoțiți felul principal cu o felie de pâine.<br/><br/>&nbsp; <br/><br/><h3>&nbsp; &nbsp; Ziua 3 - carbohidrați</h3><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Cunoscută drept ziua cadoului sau a gustului preferențial.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Mic dejun</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Micul dejun se face ca în zilele anterioare, alegând fructele care vă plac și adăugând cafea sau ceai fără zahăr.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Prânz</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Există o cantitate mare de mâncare pe care o puteți mânca în a treia zi, dar ca recomandare vă sugerez o pizza marinara, în mod natural fără mozzarella, sau de orice alt tip cu legume, dar fără mozzarella sau niciun fel de brânză sau salam. De asemenea, puteți opta pentru orice altă proteină animală. În loc de pizza, puteți mânca o farfurie cu paste cu roșii și sos de busuioc sau un alt sos simplu. Porția trebuie să fie potrivită, fără exagerare.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Cină</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Aici putem consuma un toast mare bun, o bucată de prăjitură sau 100 de grame de înghețată cu adăugarea a 10 grame de ciocolată amară în plus. În mod alternativ, puteți mânca niște cartofi copți condimentați cu puțin ulei și arome.<br/><br/>&nbsp; <br/><br/><h3>&nbsp; &nbsp; Ziua 4 - ziua vitaminelor</h3><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Mic dejun</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Micul dejun rămâne și în ziua a 4-a ca și cele anterioare, cu cafea sau ceai fără adăugare de zahăr, cu 150 de grame de fructe la alegere, care pot fi, de exemplu, 2 mere mici, 2 banane mici sau una mare ori 2 piersici mici.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Prânz</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Aici avem o porție de salată mixtă completată cu o alegere între puțin ulei (una sau două linguri) și semințe precum chia, dovleac sau in, pe lângă legume sau migdale (5 sau 6 până la o duzină dacă sunt mici).<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Picnic</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Da, în a patra noastră zi avem o mână de fructe uscate la alegere fără a exagera porția.<br/><br/>&nbsp; <br/><br/><h4>&nbsp; &nbsp; Cină</h4><br/>&nbsp; <br/><br/>&nbsp; &nbsp; O salată de fructe cu adaos de unele nuci, dar fără zahăr sau o salată de fructe (pere, mere și nuci) fără ulei.<br/><br/>&nbsp; <br/><br/><h3>&nbsp; &nbsp; Ziua 29 - Ziua apei</h3><br/>&nbsp; <br/><br/>&nbsp; &nbsp; Un ultim hop de trecut al dietei Rina este <b>Ziua 29</b> - este un reper important al programului, întrucât ar trebui să poți petrece o zi întreagă consumând doar lichide și să nu mănânci altceva. Unii oameni nu pot trece prin această zi, dar cel puțin ar trebui să încercați.<br/><br/>&nbsp; </div>

## &nbsp; &nbsp; Dieta Rina: concluzii

  
&nbsp;   
  
&nbsp;   

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-MLFAvcwI1hg/XkZWVYcCZHI/AAAAAAAAEBU/-ekkMJxFbOAyjRdnE68oYF__tongcfFhgCPcBGAYYCw/s1600/question-doubt-problem-mark-3d0c511fc62346dc178ba3c517b8b3ba.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="979" data-original-width="1600" height="195" src="https://1.bp.blogspot.com/-MLFAvcwI1hg/XkZWVYcCZHI/AAAAAAAAEBU/-ekkMJxFbOAyjRdnE68oYF__tongcfFhgCPcBGAYYCw/s320/question-doubt-problem-mark-3d0c511fc62346dc178ba3c517b8b3ba.jpg" width="320"/></a></div>

Este o dietă care propune un angajament de trei luni, o perioadă lungă dacă vă gândiți la dietele care promit pierderi spectaculoase în greutate&nbsp; în una sau două săptămâni. Desigur, slăbind 20 până la 25 kg în trei luni înseamnă aproximativ 2 kg de pierdere în greutate pe săptămână, cu siguranță o subțiere prea rapidă, recomandarea e că dieta să meargă mână în mână cu activitatea fizică.  
  
  

<div class="separator" style="clear: both; text-align: center;"></div>

  
  
&nbsp; &nbsp; Din dieta Rina notăm interesul pentru fructe și legume, apoi trebuind să descoperim în dieta noastră cereale mai puțin cunoscute, cum ar&nbsp; teff, orz, ovăz, mei și secară. Această dietă nu este totuna cu dietele disociate, alimentele sunt complexe din punct de vedere nutrițional, nu este posibil să separi realmente carbohidrații de proteine, însă trebuie să încercăm să controlăm aportul de proteine provenite din paste și pâine.  
  
&nbsp;   
  
&nbsp; &nbsp; În dieta Rina, conceptul zilei dedicate postului este de asemenea interesant, mai ales într-o eră a alimentelor exagerate precum a noastră, chiar dacă 24 de ore de restricție calorică totală sunt multe și dificil de realizat fără a suferi. Personal, cred într-o abordare mai puțin drastică, cum ar fi postul intermitent până la 16 ore,&nbsp; multe studii confirmând eficacitatea acestuia.  
  
&nbsp;   
  
&nbsp; &nbsp; Faptul că nu trebuie să numărăm caloriile alimentelor pe care le consumăm este benefic, deoarece contorizarea caloriilor nu se poate face pe viață, iar orice dietă care&nbsp; propune asta o consider ineficientă. Ca adepți ai unei diete variate și echilibrate, cu un amestec de exerciții și un stil de viață activ și dinamic, considerăm că dieta Rina poate servi persoanelor care caută o dietă bazată pe obiceiuri alimentare nutritive diferite și oferă un stimul psihologic suplimentar.  
  
&nbsp;   
  
&nbsp; &nbsp; Un alt avantaj al dietei Rina este că urmând-o&nbsp; puteți slăbi fără a vă face griji pentru temutul efect al revenirii dietetice, datorită îmbunătățirii metabolismului.  
  
&nbsp;   
  

## &nbsp; &nbsp; Reguli de urmat în dieta Rina

  
&nbsp;   
  
&nbsp; &nbsp; Nu trebuie să existe nici o renunțare sau sărit peste mese, ci doar să includeți alimentele oferite de planul nutrițional în ziua respectivă.  
  
&nbsp;   
  
&nbsp; &nbsp; Nu exagera cu limitele de calorii sau cantitatea de alimente care poate fi consumată în timpul unei zile, deoarece secretul este în combinația potrivită de alimente.  
  
&nbsp;   
  
&nbsp; &nbsp; Trebuie urmărite intervalele de timp stabilite și distribuirea corectă a meselor.  
  
&nbsp;   
  
&nbsp; &nbsp; Unele fructe se mănâncă în fiecare zi cu o dietă.  
  
&nbsp;   
  
&nbsp; &nbsp; Cina trebuie să aibă loc cel târziu la ora 21:00, dar este recomandat să fie până la 20:00, deoarece în timpul nopții oamenii ard mai puține calorii.  
  
&nbsp;   
  
&nbsp; &nbsp; Cantitatea de mâncare la prânz va fi dublă față de cei de la cină.  
  
&nbsp;   
  
&nbsp; &nbsp; Între mesele principale trebuie să treacă cel puțin 3 ore (mic dejun, prânz și cină)  
  
&nbsp;   
  
&nbsp; &nbsp; Nici o cantitate de alcool sau cofeină cu zahăr nu poate fi consumată în primele 90 de zile.  
  
&nbsp;   
  
&nbsp; &nbsp; Pentru a accelera procesul, este necesar să beți cel puțin doi litri de lichide pe zi, poate fi apă sau infuzii, însă fără zahăr.  
  
&nbsp;   
  
&nbsp; &nbsp; În data de 29 a fiecărei luni puteți consuma doar lichide, așa cum am menționat deja mai sus.  
  
&nbsp;   
  
&nbsp; &nbsp; Dieta Rina NU ESTE O DIETĂ RESTRICTIVĂ,  

<div class="separator" style="clear: both; text-align: right;"><a href="https://lh3.googleusercontent.com/-3rvIJAVdEwY/XlLy_YiwItI/AAAAAAAAEKk/UNTV5hPlAocVYtNBLTkp-cBeBghH0OIEwCLcBGAsYHQ/s1600/Dieta%2BRina%2Bword%2Bcloud.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" data-original-height="768" data-original-width="1024" height="300" src="https://lh3.googleusercontent.com/-3rvIJAVdEwY/XlLy_YiwItI/AAAAAAAAEKk/UNTV5hPlAocVYtNBLTkp-cBeBghH0OIEwCLcBGAsYHQ/s400/Dieta%2BRina%2Bword%2Bcloud.jpg" width="400"/></a></div>

astfel încât să puteți mânca dintr-o varietate de alimente, dar vă învață o modalitate de a le combina în timpul zilei într-un mod adaptabil, care este mai benefic.  
  
&nbsp;   
  
&nbsp; &nbsp; Este obligatoriu să urmariți repetiția de patru zile în timpul primelor 90 de zile.  
  
&nbsp;   
  
&nbsp; &nbsp; Până la 12 noaptea sunt permise fructele.  
  
&nbsp;   
  
&nbsp; &nbsp; Nu trebuie să bei apă sau lichide înainte, în timpul sau după mese (cel puțin nu imediat) pentru a evita diluarea sucurilor gastrice. Ar trebui să bem în timpul zilei, dar în afara mesei principale. Independent de acest sfat, trebuie să asigurăm organismului un echilibru corect de apă, astfel încât să nu rețină lichidele.  
  
&nbsp;   
  
&nbsp; &nbsp; În cazul unei reacții nefavorabile, este recomandat să mergeți la un nutriționist care vă poate ajuta să vă cunoașteți metabolismul și să vă ghidați în acest proces.