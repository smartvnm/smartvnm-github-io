---
title: Dieta keto meniu
date: 2020-02-06 07:12:00 +00:00
tags:
- keto
- meniu
- dieta
layout: post
author: Gabriela
modified_time: '2020-02-22T07:04:10.494+02:00'
thumbnail: https://1.bp.blogspot.com/-tc0zmrP-2Ek/Xju8IOoc1MI/AAAAAAAAD4k/aUpHVGnXjPwyjtgMkwNcfvwooR2Ld0J4gCLcBGAsYHQ/s72-c/food-3223286.jpg
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-2413070410742240708
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/dieta-keto-meniu.html
---

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-tc0zmrP-2Ek/Xju8IOoc1MI/AAAAAAAAD4k/aUpHVGnXjPwyjtgMkwNcfvwooR2Ld0J4gCLcBGAsYHQ/s1600/food-3223286.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="1067" data-original-width="1600" height="213" src="https://1.bp.blogspot.com/-tc0zmrP-2Ek/Xju8IOoc1MI/AAAAAAAAD4k/aUpHVGnXjPwyjtgMkwNcfvwooR2Ld0J4gCLcBGAsYHQ/s320/food-3223286.jpg" width="320"/></a></div>

Te-ai saturat de diete care sa iti interzica sa mananci insa vrei sa slabesti sa sa iti recapeti silueta din trecut? Exista un concept in lumea dietelor de care ar trebui sa afli. Este vorba de dieta ketogenica, care se bazeza pe regimuri disociate.  
  

*   Istoric
*   In zilele noastre
*   Care este diferența dintre dieta Atkins și dieta Keto?
*   Diferenta dintre Keto și Paleo ?

  

*   Dieta Keto este sigură?

## Istoric

In anii 1920, oamenii de știință au descoperit că o dietă foarte bogată în grăsimi și foarte scăzută în carbohidrați ajuta la tratarea copiilor cu epilepsie. Dieta keto acționează prin privarea organismului de glucoză, care se găsește în carbohidrați. Acest lucru, la rândul său, forțeaza organismul sa converteasca grăsimea în energie, formând cetone care sunt eliberate din ficat. Acest proces de cetoză, starea metabolică în care organismul preia energia de care are nevoie din grăsimi în loc de carbohidrați, are loc, de asemenea, atunci când postim, iar organismul trebuie să se bazeze pe rezervele de grăsime pentru energie. Nu se știe sigur de ce această dietă a funcționat pentru epilepsie, dar rezultatele au fost pozitive pentru pacienți. Cand dieta keto a fost introdusă pentru prima dată în cazul copiilor cu epilepsie, ei au fost instruiți să urmeze o dietă foarte strictă. Copii au fost internați în spital pentru a putea fi monitorizați complet. Ei au început cu o perioadă de post, care a produs niveluri semnificative de cetone în organism. Ei au fost apoi tinuti pe o dieta foarte bogata în grăsimi. După 1-2 luni, mulți copii au reușit să-și reducă medicamentele pentru convulsii.    

## In zilele noastre

Dieta Keto a devenit populara pentru pierderea în greutate. Multe studii au fost efectuate pentru a cerceta efectele fiziologice ale dietei și au găsit dovezi ale eficienței sale. Un studiu special asupra efectului dietei Keto in cazul obezității / pierderii în greutate, diabetului zaharat de tip 2, bolilor cardiovasculare și acnee, și a concluzionat că rezultatele sunt promițătoare, dar e nevoie de mai multa cercetare.   

## Care este diferența dintre dieta Atkins și dieta Keto?

Atât dieta Atkins, cât și dieta keto sunt diete cu restricție de carbohidrați. O diferență cheie este că dieta Atkins permite mai multe proteine decât dieta keto. Cu Atkins, puteți mânca oricâte proteine, cu keto, cantitatea de proteine este limitată la aproximativ 20 la sută din totalul de calorii zilnice. De asemenea, cu Atkins, o dieta etapizata, carbohidratii pot fi adăugati înapoi în dieta în cele din urmă, însă la dieta keto, reintroducerea carbohidratilor ar scoate organismul din cetoză.     

## Diferenta dintre Keto și Paleo ?

Dieta paleo este o altă dietă bazată pe reducerea carbohidraților, dar nu este la fel de severă ca dieta keto. Carbohidratii taiati din dieta paleo sunt carbohidrati rafinati ai dietei moderne, pe premisa că ceea ce a fost sănătos pentru vânători și culegători in timpuri străvechi ar trebui să fie sănătos și pentru noi, oamenii moderni, de asemenea, deoarece am evoluat timp de multe mii de ani în conformitate cu acest stil de viață special. Deci, paleo permite carbohidrati sanatosi, cum ar fi legume întregi și fructe, dar renunță la alimentele procesate. Dieta keto pune mai mult accent pe grăsime în dieta.  

## Dieta Keto este sigură?

Cine vrea sa încerce dieta Keto ar trebui să se consulte cu un medic înainte de a începe. Această dietă poate pune probleme rinichilor și poate avea alte efecte negative asupra organismului, inclusiv senzații descrise ca "gripa keto " - greață, oboseală, dureri de cap și amețeli. Un nivel ridicat de cetone în sânge, împreună cu un nivel scăzut de insulină poate fi foarte nesigur, în special pentru cei cu diabet zaharat. De asemenea, dieta keto este dificil de susținut în timp, deoarece este atât de restrictivă. Pentru cei mai mulți dintre noi, mananca o varietate de alimente nutritive care ne bucurăm (inclusiv fructe, legume, cereale integrale, împreună cu obținerea o multime de exercitii, este mult mai probabil va fi un mod durabil și sănătos de a trăi. Faceti sport zilnic, alergati, miscati-va si chiar daca nu aveti chef de alergat iesiti macar intr-o plimbare in parc. Pe langa faptul ca miscarea va ajuta sa va relaxati, daca va veti obisnui sa faceti exercitii sau macar sa alergati zilnic veti observa cum corpul dumneavoastra capata niste forme deoasebit de frumoase.  
Nu uitati ca trebuie sa aveti un orar al meselor fix si ca trebuie sa va odihniti. Nu va decalati orarul de somn si odihniti-va organismul. Sub nici o forma nu uitati de micul dejun deoarece acesta va va aduce energie pentru intreaga zi.