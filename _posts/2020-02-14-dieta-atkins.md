---
title: Dieta Atkins
date: 2020-02-14 19:57:00 +00:00
tags:
- meniu
- dieta
- Atkins
layout: post
author: Gabriela
modified_time: '2020-02-22T06:58:59.270+02:00'
thumbnail: https://1.bp.blogspot.com/-_fv6CsRYcdQ/Xkb7fNHPfyI/AAAAAAAAEDY/BHj96rGOKP4llkuFr4ARe6fDNfQ58Rv0wCKgBGAsYHg/s72-c/nci-vol-2463-150.jpg
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-919088954028488121
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/dieta-atkins.html
---

### Slabire cu dieta Atkins&nbsp;

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-_fv6CsRYcdQ/Xkb7fNHPfyI/AAAAAAAAEDY/BHj96rGOKP4llkuFr4ARe6fDNfQ58Rv0wCKgBGAsYHg/s1600/nci-vol-2463-150.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" data-original-height="900" data-original-width="1350" height="213" src="{{ site.url }}/assets/2020-02-14-image-0000.jpg" width="320"/></a></div>

  
&nbsp; &nbsp;Experții concep diferite tipuri de dietă pentru ca oamenii să-și atingă greutatea ideală. Una din multele diete pe care le putem folosi este dieta Atkins.&nbsp; Cei care aleg să urmeze dieta Atkins trebuie sa reducă aportul de carbohidrați la doar 30 de grame pe zi.  
  
Dieta Atkins se bazează pe ideea că persoanele supraponderale consumă prea mulți carbohidrați. Corpurile noastre ard atât carbohidrații, cât și grăsimile pentru energie. Carbohidrații sunt folosiți mai întâi. Reducând aportul de carbohidrați și consumând mai multe proteine ​​și grăsimi, organismul e nevoit să slăbească prin arderea grăsimilor depozitate mai eficient.  
  
Dieta Atkins este cunoscută și sub denumirea de Atkins Nutritional Approach. Este cea mai comercializată și cunoscută dietă cu conținut redus de carbohidrați. Acesta a fost dezvoltat de Dr. Robert Atkins în anii 1960, dintr-o dietă despre care a citit în Journal of The American Medical Association. El a continuat apoi să-și rezolve propria problemă de greutate la scurt timp după absolvirea școlii de medicină. După ce a tratat cu succes peste zece mii de pacienți, a început să popularizeze dieta Atkins într-o serie de cărți. Prima dintre acestea a fost Dr. Atkins - Diet Revolution în 1972. Apoi a revizuit această carte, numită Dr. Atkins - New Diet Revolution.  
  
S-a creat, de asemenea, o afacere care să ofere produse dietetice pentru persoanele care urmeaza dieta Atkins. Această franciză a avut un mare succes datorită comercializării dietei.  

<table cellpadding="0" cellspacing="0" class="tr-caption-container" style="float: left; margin-right: 1em; text-align: left;"><tbody><tr><td style="text-align: center;"><a href="https://1.bp.blogspot.com/-dS5fK8wgYdc/XkbyCVg8hAI/AAAAAAAAEDI/Nb_Sp-2AVfITt3pT5d26xhAmAtxZSBQIgCPcBGAYYCw/s1600/84c8c5005af1ec8f58a1f5164e3e910f.gif" imageanchor="1" style="clear: left; margin-bottom: 1em; margin-left: auto; margin-right: auto;"><img border="0" data-original-height="453" data-original-width="400" height="400" src="{{ site.url }}/assets/2020-02-14-image-0001.gif" width="352"/></a></td></tr><tr><td class="tr-caption" style="text-align: center;">Piramida alimentelor Atkins</td></tr></tbody></table>

  
&nbsp; Dr. Atkins a afirmat că există doi factori principali nerecunoscuți atunci când vine vorba de obiceiurile alimentare occidentale. Prima cauză principală a obezității este consumul de prea mulți carbohidrați rafinați. Aceasta include făina, zahărul, siropurile de porumb, fructoza bogată și grăsimile saturate. El a afirmat că trebuie evitate doar grăsimile trans, care provin din uleiurile hidrogenate.  
  
&nbsp; Dr. Atkins a respins sfaturile din piramidele alimentare, afirmând că creșterea glucidelor rafinate este responsabilă de creșterea tulburărilor metabolismului în secolul XX. Se presupune că asa-zisele efectele dăunătoare ale grăsimilor alimentare au accentuat problema obezității, în loc să o diminueze.  
  
  
&nbsp; &nbsp;Punctul forte al dietei Atkins este că dacă organismul nu obține combustibilul necesar de la carbohidrați, l-ar obține din alte surse. Și ca atare, va transforma grăsimile pentru a obține energia necesară.  

<div class="separator" style="clear: both; text-align: center;"><br/></div>

  

### Etapele dietei

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-DeLfEIvz1Xc/Xkb676HIEGI/AAAAAAAAEDQ/xg1Qy4Co85cdIbJE5oF3-Yc-Bzi7C1_bACEwYBhgL/s1600/woman-1979272_1920.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" data-original-height="1067" data-original-width="1600" height="213" src="{{ site.url }}/assets/2020-02-14-image-0002.jpg" width="320"/></a></div>

  
&nbsp; Dieta Atkins este împărțită în patru faze:  
  
&nbsp;Prima fază se numește inducție, în care poți mânca puțin sau deloc carbohidrați pe toată durata ei. Cantitatea maximă de carbohidrați pe care o puteți consuma este de numai 20 de grame pe zi.  
  
&nbsp;Faza a doua se numește "faza&nbsp; de scădere în greutate". În această fază, puteți pune încet înapoi glucide în dieta dumneavoastră. De la 20 de grame în timpul fazei de inducție, puteți lua până la 25 de grame.  
  
&nbsp;A treia fază se numește faza de pre-întreținere. Aici, faci tranziția de la a pierde kilograme spre menținerea greutății. În timpul fazei de pre-întreținere, puteți adăuga încă 5 grame la aportul de carbohidrați, adica 30 de grame pe zi.  
  
&nbsp; A patra și ultima fază se numește întreținere pe viață. Aceasta este faza pe care o veti păstra tot timpul. Aveți voie să mancati tot felul de alimente&nbsp; atâta timp cât pastrati aportul de carbohidrați. În mod ideal, nu ar trebui să treci de 30-35g.  
  

### Concluzie

  
  
Dieta Atkins implică o restricție a carbohidraților, astfel încât acesta va schimba metabolismul organismului de la arderea glucozei la arderea proteinelor și a grăsimilor. Acest proces este cunoscut sub numele de lipoliză. Acest fenomen începe atunci când organismul intră într-o stare de cetoză, din cauza eliminării excesului de carbohidrați pentru a arde. Conform dietei Atkins, dietele cu conținut scăzut de carbohidrați au un „avantaj metabolic”, deoarece organismul arde mai multe calorii decât în ​​alte diete.  
  
Folosind dieta Atkins, puteți arde de fapt mai multe calorii, fără să faceți întotdeauna eforturi mai mari și să vă transformați corpul într-o mașină de ardere a grăsimilor. Cel mai bun dintre toate, nu trebuie să simțiți foame sau foame, și totuși sa ardeti grăsime nedorită.