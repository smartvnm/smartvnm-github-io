---
title: 'Slăbeste cu ceaiul Oolong '
date: 2020-02-09 16:33:00 +00:00
tags:
- ceai
- slăbit
- oolong
layout: post
author: Gabriela
modified_time: '2020-02-22T07:00:24.644+02:00'
thumbnail: https://1.bp.blogspot.com/-0f5KbN7eNTw/XkA3D16Wj2I/AAAAAAAAD9o/LE9vi7csW2MKuFkYflaN1_2_rKJm_wVnACLcBGAsYHQ/s72-c/single-clump-tea-oolong-tea-areas-fragrance-2a4186e57b2a78bf10587435220156e3.jpg
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-134963124828307553
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/slabit-cu-ceaiul-oolong.html
---

Există atât de mult interes față de diete bazate pe ceai oolong (scris uneori wulong) așa că vom analiza cercetarea care sprijină conexiunea dintre slăbit și acest tip de ceai.  
  
Să începem cu începutul.  

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-0f5KbN7eNTw/XkA3D16Wj2I/AAAAAAAAD9o/LE9vi7csW2MKuFkYflaN1_2_rKJm_wVnACLcBGAsYHQ/s1600/single-clump-tea-oolong-tea-areas-fragrance-2a4186e57b2a78bf10587435220156e3.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="1200" data-original-width="1600" height="240" src="{{ site.url }}/assets/2020-02-09-image-0000.jpg" width="320"/></a></div>

  
Ceaiul oolong provine dintr-o plantă numita Camellia Sinensis, dacă nu provine de la planta aceasta, nu este ceai (înseamnă că ierburile provin dintr-o varietate de plante). De-a lungul timpului, planta s-a protejat de stresori fotosintetici prin formarea de compuși chimici cunoscuți sub numele de polifenoli. Polifenolii, care includ flavonoidele, au aceeași clasă benefică de compuși, antioxidanții, care fac fructele și legumele bune pentru tine.  
  
Diferența dintre ceai verde și ceai oolong este de prelucrare. Tot ceaiul e verde când e cules. Ceaiul verde este încălzit pentru a opri reacția enzimatică naturală (oxidare) a frunzei. Odată uscate, frunzele de ceai verde sunt apoi rasucite în mod intenționat, rupând structura celulei. Frunzele de ceai Oolong sunt culese, păstrate în condiții atent controlate și lăsate să se oxideze. Aceste frunze nu sunt rupte intenționat, lăsând cea mai mare parte a structurii celulei intacte. Aceste diferențe de prelucrare fac ca fiecare categorie de ceai să fie în mod benefic diferită, chiar dacă provin din aceeași plantă.  
  

### Legătura Cu Scăderea În Greutate

  
&nbsp; Cele două modalități principale de reducere a greutății corporale legate de alimentatie sunt:    
  
cresterea cheltuielilor cu energia (EE)   
  
inhibarea absorbției nutrienților, inclusiv a grăsimilor și a carbohidraților.  
  
Cafeina este un stimulent,&nbsp; este larg acceptat faptul că cafeina în ceai crește metabolismul, prin urmare crește EE. Deci, apare întrebarea, este doar cofeina sau sunt alți compuși în ceai care contribuie la această creștere?  
  

### Cercetarea

  
Chinezii au crezut mult timp că ceaiul oolong este benefic în reducerea și menținerea greutății. Un studiu chinezesc, în 1998, efectuat pe 102 femei, a arătat că consumul continuu de ceai oolong timp de șase săptămâni a dus la o scădere a greutății corporale. Acest studiu, împreună cu problema altor compuși care contribuie la beneficiile pierderii în greutate a ceaiului, au stimulat cercetări suplimentare.  
  
În 2001, Fiziolog Dr. William Rumpler, de NE Agricultură Servicii de Cercetare' Dieta Omului și de Laborator, a investigat convingerea antica a chinezilor ca ceaiul oolong este eficient in controlul greutatii corporale. Studiul a măsurat modul în care ceaiul influențează cheltuielile energetice (EE) și a inclus 12 voluntari de sex masculin cărora li s-au administrat 4 formule separate de băuturi timp de trei zile consecutive. Preparatele de băuturi au constat în:  
1) ceai oolong,  
2) apă cafeinizată cu cafeină, egală cu ceai oolong,  
3) Ceai oolong cu jumătate de concentrație și  
4) apă fără cafeină.  
  
După 24 participanților le-a fost măsurat EE și rezultatele au fost următoarele:  
  
- Niveluri EE cu aproximativ 3% mai mari atunci când au băut ceai oolong sau apa cofeinizată față de apă fără cofeina.  
  
- Participanții au ars în medie cu 67 de calorii mai mult pe zi când au băut tot ceaiul oolong.  
  
- Oxidarea grăsimilor (arderea grăsimilor)&nbsp; a crescut cu 12% după consumarea completă a ceaiului oolong față de apa cafeinizată.  
  
- Aceste date confirmă faptul că o componentă, alta decât cofeina, este responsabilă pentru utilizărea preferențiala a grăsimii ca sursă de energie.  
  
&nbsp;Oamenii de știință au speculat apoi că cofeina combinată cu EGCGs au lucrat împreună pentru a crește arderea grăsimilor.  
Un studiu japonez, realizat în 2003, a mers un pas mai departe comparând beneficiile ceaiului Oolong și ceaiului verde privind reducerea greutății. Unsprezece tinere studente sănătoase au participat la acest studiu bine controlat. Participanții au primit trei formule diferite de băutură:  
  
&nbsp;1) ceai oolong,  
&nbsp;2) frunze de ceai verde sub formă de pulbere și  
&nbsp;3) apă.  
  
Ambele ceaiuri erau preparate cu apă fiartă. Ceaiul oolong s-a îmbibat cinci minute și frunzele de ceai verde s-au dizolvat.  
  
După efectuarea măsurătorilor, rezultatele determinate;  
  
- Ceaiul Oolong a avut niveluri mai mari de EE de la început până la sfârșit și la intervale de 30, 60, 90 și 120 de minute.  
  
- Nivelul EE a atins un maxim dupa 90 de minute atât pentru oolong cât și pentru ceaiul verde și a rămas la același nivel până la 120 de minute.  
  
- Aceste rezultate indică faptul că după consumul de ceai oolong se va consuma mai multă energie decât dacă s-ar bea ceai verde sau apă și că efectul poate dura până la două ore.  
  
&nbsp;Concentratiile de cafeină, catecine individuale și alți polifenoli au fost, de asemenea, măsurate producând aceste constatări interesante  
  
- Cofeina și conținutul de EGCG a fost mult mai mari în ceai verde față de ceai oolong.  
  
- Concentrația de polifenoli polimerizați a fost semnificativ mai mare în ceaiul oolong față de ceaiul verde.  
  
Aceste constatări arată că&nbsp; polifenolii polimerizati, in cantitate mai mare în ceai oolong, sunt cei care leagă ceaiul de arderea grăsimilor, nu doar cofeina sau doar combinația de cafeină și EGCGs. În plus, restul compușilor comparați în ceaiuri erau similari sau egali între ei, fără diferențe marcate, consolidând rezultatele.  
  
Am arătat că ceaiul oolong crește semnificativ EE pentru până la 120 de minute și poate chiar promova utilizarea preferențială a grăsimii ca sursă de energie, ceea ce o face un instrument sănătos pentru reducerea și menținerea greutății. Cum rămâne cu absorbția nutrienților?  
  
Se consideră că efectul ceaiului Oolong asupra blocării absorbției grăsimilor și carbohidraților joacă un rol-cheie în calitățile sale de reducere a greutății. În timp ce sunt necesare mai multe studii, studiile inițiale indică faptul că ceaiul oolong are potențial de blocare a absorbției.  
  
Există metode dovedite de a pierde în greutate și de a o ține departe; exerciții fizice, dormit suficient și a o dietă echilibrată. Ceaiul Oolong poate îmbunătăți aceste schimbări pozitive și poate obține rezultate mai rapide.  

<div class="separator" style="clear: both; text-align: center;"></div>

<a name="more"></a><a href="https://1.bp.blogspot.com/-0HB1UQ8E8RE/XkA3UTIisuI/AAAAAAAAD9w/MHeezUf-OE0Q4vlsjKnWojVlpE8vscjQACLcBGAsYHQ/s1600/oolong-tea-set-tea-teapot-25e4162767b75c8ef02dcdc2db2e7596.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;">

<img border="0" data-original-height="900" data-original-width="1600" height="180" src="{{ site.url }}/assets/2020-02-09-image-0001.jpg" width="320"/>

</a>  
  
  
Să recunoaștem.  
  
- Ceaiul Oolong&nbsp; nu este un remediu rapid în ciuda reclamelor profesioniștilor in marketing.  
  
- Ceaiul Oolong nu trebuie să fie scump pentru a fi eficient.  
  
- Aceste studii nu indică faptul că oolong cultivat într-o anumită zonă este mai bun&nbsp; decât alt oolong. Procesarea contează.  
  
- Pierderea a 20 de kilograme în 30 de zile prin adăugarea a două cești de ceai oolong la rutina de zi cu zi nu este nimic mai mult decât hype. Dacă ar fi adevărat, aș purta mărimea XS.  
  
- Dacă sună prea bine ca să fie adevărat, atunci e prea bun ca să fie adevărat!  
  
Oolong - Realitatea  
  
&nbsp;-Ceaiurile Oolong au un gust grozav și sunt delicioase, calde sau reci.  
  
- Există o varietate imensă de ceai oolong la prețuri accesibile.  
  
- Bucurați-vă de o ceașcă de oolong 30 de minute până la o oră înainte de a merge la antrenament sau la yoga și culege recompensele!  
  
- Bea oolong după-amiaza pentru a opri mai târziu pofta de dulce și căderile de energie.  
  
- Toate aceste ajutoare pentru o scădere în greutate și întreținere sănătoase