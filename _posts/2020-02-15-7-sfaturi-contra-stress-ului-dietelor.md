---
title: 7 sfaturi contra stress-ului dietelor
date: 2020-02-15 03:25:00 +00:00
tags:
- sfaturi
- dieta
- stress
layout: post
author: Gabriela
modified_time: '2020-02-22T06:58:24.236+02:00'
thumbnail: https://1.bp.blogspot.com/-YGNejiPEhF4/XkdkqZQVkqI/AAAAAAAAEEM/MO3I2J77OFcGkXApHjdIeFf1Nq63uDxjACKgBGAsYHg/s72-c/model_man_human_self_portrait_show_off_posers_important_cool-1097411.jpg
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-8288139666459739934
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/7-sfaturi-contra-stress-ului-dietelor.html
---

### Stresul îți afectează lupta cu kilogramele?

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-YGNejiPEhF4/XkdkqZQVkqI/AAAAAAAAEEM/MO3I2J77OFcGkXApHjdIeFf1Nq63uDxjACKgBGAsYHg/s1600/model_man_human_self_portrait_show_off_posers_important_cool-1097411.jpg" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><img border="0" data-original-height="800" data-original-width="1200" height="213" src="https://1.bp.blogspot.com/-YGNejiPEhF4/XkdkqZQVkqI/AAAAAAAAEEM/MO3I2J77OFcGkXApHjdIeFf1Nq63uDxjACKgBGAsYHg/s320/model_man_human_self_portrait_show_off_posers_important_cool-1097411.jpg" width="320"/></a></div>

  
&nbsp; &nbsp;Stresul cauzat de dietă se adaugă la stresul personal și la viața profesională cu care ne confruntăm în fiecare zi, rezultând adesea într-un "ciclu vicios" de stres sporit și consum sporit de alimente. Oamenii sub stres tind să aiba&nbsp; comportamente nesănătoase, cum ar fi crănțănitul și există dovezi biologice puternice că oamenii stresați pun kilograme mai repede.  
  

#### &nbsp;"Sunt stresat, așa că mănânc."

  
&nbsp; Mulți oameni se plâng că mănâncă prea mult ca răspuns la stresul la locul de muncă sau stresul personal. Dacă te recunoști, citește următoarele sfaturi pentru a gestiona mai bine situatiile dificile:  
  
1. Exersează așteptarea. Amânați-vă satisfacția instantanee când vă lovește foamea. Spune-ți că vei aștepta între 10 și 30 de minute să mănânci. Dacă poftele tale sunt legate doar de stres, vor dispărea când îți permiți să fii distras.  
  
2. Ține un jurnal cu ce mănânci în fiecare zi. Știind că trebuie să scrii fiecare gustare sau ciuguleala te poate face sa gândești înainte de a mânca.  
  
3. Cere ajutorul unui prieten-ideal, unul care, de asemenea, este tentat de pofte legate de stres. Când vrei să mănânci, scrie-i un bilet sau dă un telefon.  
  
4. Ține-te hrănit. Mănânca&nbsp; mese normale, sănătoase, pentru a menține nivelul de energie. Săritul peste mese, fiind sub stres, te încurajează să mănânci mai mult, și nesănătos.  
  
5. Post-It-uri cu mesaje lipite in zone unde&nbsp; te apuca foamea. Folosește orice mesaj care funcționează pentru tine. Exemple ar putea fi "chiar ți-e foame?"sau" gândește-te de ce mănânci."  
  
6. Scoateți din casă sau din birou produse alimentare tentante. Nu mergeți la cumpărături cand vă e foame și faceti întotdeauna o listă de produse alimentare înainte de cumpărături.  
  
7. Fă-ți timp pentru tine în fiecare zi. Fie că este vorba de 10 minute pentru a reflecta și relaxa, sau dacă este o activitate distractivă care te va revitaliza, este esențial să o faci. Sănătatea nu este doar fizică, bunăstarea mentală este la fel de importantă, meriți să te eliberezi mental de stres în fiecare zi!