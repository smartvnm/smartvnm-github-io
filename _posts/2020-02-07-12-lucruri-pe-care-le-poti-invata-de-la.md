---
title: 12 Lucruri Pe Care Le Poți Învăța De La Un Copil De Doi Ani
date: 2020-02-07 21:04:00 +00:00
tags:
- sfaturi
- copii
- slăbit
layout: post
author: Gabriela
modified_time: '2020-02-22T07:01:57.941+02:00'
thumbnail: https://1.bp.blogspot.com/-QjphhPXOH0c/Xj3QyC9txbI/AAAAAAAAD8A/xf-swMzOvfgqhVJaNMeIitldArzony7gACLcBGAsYHQ/s72-c/toddler-3995508_1920.jpg
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-5486252256228249759
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/12-lucruri-pe-care-le-poti-invata-de-la.html
---

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-QjphhPXOH0c/Xj3QyC9txbI/AAAAAAAAD8A/xf-swMzOvfgqhVJaNMeIitldArzony7gACLcBGAsYHQ/s1600/toddler-3995508_1920.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="1067" data-original-width="1600" height="426" src="{{ site.url }}/assets/2020-02-07-image-0000.jpg" width="640"/></a></div>

Dacă întrebi un copil despre adulții din jurul lor, probabil ar spune că sunt puțin nebuni și par a fi stresați mai mereu.  
&nbsp;Iată câteva lucruri simple pe care ne pot învăța...  
&nbsp;1. Dormi când ești obosit.  
&nbsp;2. Mănâncă când ți-e foame.  
&nbsp;3. Nu te înfometa, te obosește și te irită. Mănâncă bucățele mici de multe ori pentru a menține sătul.  
&nbsp;4. Încăpățânarea  
Refuza să mănânci chiar și încă o înghițitură odată ce ești plin. Dacă ești plin, aruncă cu bucurie restul.  
&nbsp;5. Pleacă de la masa cu o burtă plină și satisfăcută și cu dorința de a te cufunda inapoi în viața voastră delicioasă.  
&nbsp;6. Fii mofturos și mănâncă doar ce-ți place. Dacă nu-ți place, închide-ți gura și refuză să mănânci până nu găsești ceva mai bun.  
&nbsp;7. Fii uimit de cât de uimitor și minunat este corpul tău.  
&nbsp;8. Fugi, sari, joacă-te. Mișcă-ți corpul pentru că e atât de distractiv și te simti bine. Fii uimit de toate lucrurile fizice incredibile pe care le poate face&nbsp;corpul tău.&nbsp;   
9. Poartă haine confortabile si asta te va se face să te simți bine.  
&nbsp;10. Apreciaza oamenii din jurul tău pentru cine sunt, mai degrabă decât pentru modul în care arată.  
&nbsp;11. Stai cu oameni distractivi, prietenoși și stai departe de cei răi și critici.  
&nbsp;12. Te simți minunat pentru că, ei bine, de ce nu ai face-o?   Dacă nu aveți un copil de doi ani în jur, cautati unul și urmăriți-l pentru o zi. Simpla lor bucurie de viață, și respectul lor absolut pentru propriile lor corpuri este ceva la care să aspiram.