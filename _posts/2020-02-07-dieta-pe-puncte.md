---
title: Dieta pe puncte
date: 2020-02-07 20:40:00 +00:00
tags:
- puncte
- dieta
layout: post
author: Gabriela
modified_time: '2020-02-22T07:02:31.207+02:00'
thumbnail: https://1.bp.blogspot.com/-0C4s1lamJ6M/Xj3A6reMIwI/AAAAAAAAD68/42OfGNfIEsAvXL5pSJovFk8qeK6eLObXACLcBGAsYHQ/s72-c/woman-1979272_1920.jpg
blogger_id: tag:blogger.com,1999:blog-6209706096907896857.post-1204675259600390003
blogger_orig_url: https://www.cumsaslabesti.com/2020/02/dieta-pe-puncte.html
---

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-0C4s1lamJ6M/Xj3A6reMIwI/AAAAAAAAD68/42OfGNfIEsAvXL5pSJovFk8qeK6eLObXACLcBGAsYHQ/s1600/woman-1979272_1920.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" data-original-height="1067" data-original-width="1600" height="213" src="{{ site.url }}/assets/2020-02-07-image-0000.jpg" width="320"/></a></div>

Vreti sa aratati mai bine si sa va imbunatatiti stilul de viata? Nu mai suportati imaginea pe care o arata cantarul si ati reusit sa va decideti si sa va mobilizati pentru a incepe o&nbsp;noua dieta? Atunci sunteti pe calea cea buna.  
&nbsp;Astazi vom aduce in discutie un alt tip de dieta, dieta pe puncte.&nbsp; Acesta dieta a fost creata si testata chiar de catre un medic contemporan american care a reusit sa dovedeasca faptul ca aceasta dieta are succes si ca da cu adevarat&nbsp;rezultate benefice.    Aceasta dieta va ajuta sa slabiti foarte usor si fara prea mult efort.  
&nbsp;Cu aceasta dieta nu veti simti nicio povara a regimului si in plus dupa foarte putin timp va veti convige de faptul ca a fost cu adevarat benefica.    Dieta care se bazeaza pe principiul&nbsp;punctelor&nbsp;se refera la faptul ca trebuie sa calculati in permanenta punctele alimentare pe care le consumati zilnic in raport cu greutatea pe care o aveti.  
&nbsp;Zilnic trebuie sa consumati puncte dupa cum urmeaza:  
&nbsp;1. daca aveti greutatea pana in 70 de kilograme atunci numarul de puncte este de 18;  
2.  pentru o greutate intre 71 si 80 de kilograme 20 de puncte,  
3.  intre 81 si 90 de kilograme 22 de puncte,  
4.  iar intre 91 de kilograme si 100 de kilograme 24 de puncte.  
&nbsp; Exista anumite&nbsp;alimente&nbsp;care au punct nul, adica zero puncte ceea ce inseamana ca pot fi consumate oricand si fara limita in acest tip de regim.  

<div class="separator" style="clear: both; text-align: center;"><a href="https://1.bp.blogspot.com/-jeNuCli2B44/Xj3LCYAWZ9I/AAAAAAAAD7k/cMxCO2J_AmswN4gQBuSZJIXE7SKCLd7RgCLcBGAsYHQ/s1600/weight-watchers-zero-points-foods.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" data-original-height="840" data-original-width="405" height="640" src="{{ site.url }}/assets/2020-02-07-image-0001.jpg" width="307"/></a></div>

  
&nbsp; Fructele ca portocale, afine, fragi, capsuni, caise dar si fructe in stare deshidratata, legume precum vinete, telina, dovlecei, ceapa, conopida.  
&nbsp;Bauturile care au 0 puncte si pot fi consumate mereu fara sa va faceti griji ca va veti ingrasa sunt ceaiul de plante, cafeaua si apa.  
&nbsp;Incercati aceasta dieta si vedeti daca se potriveste cu&nbsp;&nbsp;stilul dumneavoastra de viata.    

<div><br/></div>